<?php
declare(strict_types=1);

class IndexController extends ControllerBase
{

    public function indexAction()
    {
        $url = "https://rickandmortyapi.com/api/character"; // Se guarda la url en una variable
        $json = file_get_contents($url);
        $datos = json_decode($json,true); // Se decodifica la informacion del json para ser manipulada en la variables.

        $contador = $datos["info"]["count"];// contador de registros en el JSON
        $paginas = $datos["info"]["pages"];//Contador de paginas que maneja el JSON
        $rutas = $datos["info"]["next"];// URL del siguiente JSON a necesitar

        $cont = 0;
        $contPage = 1;
		
		//Se declaran los arreglos
        $episode = array();
        $statusEpisode = array();
        $genderEpisode = array();
        $imageEpisode =array();
		//For para añadir los items en los arreglos a utilizar en la pagina
        for ($i = 0; $i < $contador; $i++) {
            
            if($cont == 20){
                $contPage = $contPage + 1;
                $url = strval($datos["info"]["next"]);
                $json = file_get_contents($url);
                $datos = json_decode($json,true);
                $cont = 0;
            }

            array_push ( $episode , $datos["results"][$cont]["name"]);
            array_push ( $statusEpisode , $datos["results"][$cont]["status"]);
            array_push ( $genderEpisode , $datos["results"][$cont]["gender"]);
            array_push ( $imageEpisode , $datos["results"][$cont]["image"]);
            $cont = $cont + 1;
            
        }
		//Se envian las variables a la vista
        $this->view->contador = $contador;
        $this->view->episode = $episode;
        $this->view->statusEpisode = $statusEpisode;
        $this->view->genderEpisode = $genderEpisode;
        $this->view->imageEpisode = $imageEpisode;
    }

}

