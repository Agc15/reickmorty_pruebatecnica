<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;


class EpisodeController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    /**
     * Metodo para buscar todos los capitulos en la base de datos.
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, 'Episode', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $episode = Episode::find($parameters);
        if (count($episode) == 0) {
            $this->flash->notice("The search did not find any episode");

            $this->dispatcher->forward([
                "controller" => "episode",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $episode,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {
		

    }
	
	 /**
     * Metodo para poblar la base de datos con la API.
     */
	public function apiAction()
	{
		$url = "https://rickandmortyapi.com/api/character"; // Tomamos la URL
        $json = file_get_contents($url); 
        $datos = json_decode($json,true); // Se decodifica el JSON para manipular con variables 

        $contador = $datos["info"]["count"]; //Cantidad de capitulos del json
        $paginas = $datos["info"]["pages"]; // Numero de paginas del JSON
        $rutas = $datos["info"]["next"]; // URL del siguiente JSON

        $cont = 0; // Contador para saber cuando cambiar de URL, la api acomoda de 20 registros por URL en formato JSON
        $contPage = 1; // Contador del numero de paginas

		//Se declaran los arreglos a utilizar
        $episode = array();
        $genderEpisode = array();
        $specieEpisode =array();

		//For que recorre todos los item del json y los almacena en los arreglos declarados
        for ($i = 0; $i < $contador; $i++) {
            if($cont == 20){
                $contPage = $contPage + 1;
                $url = strval($datos["info"]["next"]);
                $json = file_get_contents($url);
                $datos = json_decode($json,true);
                $cont = 0;
            }
            array_push ( $episode , $datos["results"][$cont]["name"]);
            array_push ( $genderEpisode , $datos["results"][$cont]["gender"]);
            array_push ( $specieEpisode , $datos["results"][$cont]["species"]);
            $cont = $cont + 1;
        }
		// For que nos permite poblar la base de datos con la informacion recolectada del JSON.
		for ($i = 0; $i < 25; $i++) {
			$episodio = new Episode();
			$episodio->episode = $episode[$i];
			$episodio->gender = $genderEpisode[$i];
			$episodio->specie = $specieEpisode[$i];
			$episodio->save();
		}
	}

    /**
     * Metodo que permite editar la informacion de un registro
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $episode = Episode::findFirstByid($id);
            if (!$episode) {
                $this->flash->error("episode was not found");

                $this->dispatcher->forward([
                    'controller' => "episode",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $episode->id;

            $this->tag->setDefault("id", $episode->id);
            $this->tag->setDefault("episode", $episode->episode);
            $this->tag->setDefault("gender", $episode->gender);
            $this->tag->setDefault("specie", $episode->specie);
            
        }
    }

    /**
     * Metodo que permite guardar la informacion de un registro
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "episode",
                'action' => 'index'
            ]);

            return;
        }

        $episode = new Episode();
        $episode->episode = $this->request->getPost("episode");
        $episode->gender = $this->request->getPost("gender");
        $episode->specie = $this->request->getPost("specie");
        

        if (!$episode->save()) {
            foreach ($episode->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "episode",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("episode was created successfully");

        $this->dispatcher->forward([
            'controller' => "episode",
            'action' => 'index'
        ]);
    }

    /**
     * Metodo que salva la informacion 
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "episode",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $episode = Episode::findFirstByid($id);

        if (!$episode) {
            $this->flash->error("episode does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "episode",
                'action' => 'index'
            ]);

            return;
        }

        $episode->episode = $this->request->getPost("episode");
        $episode->gender = $this->request->getPost("gender");
        $episode->specie = $this->request->getPost("specie");
        

        if (!$episode->save()) {

            foreach ($episode->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "episode",
                'action' => 'edit',
                'params' => [$episode->id]
            ]);

            return;
        }

        $this->flash->success("episode was updated successfully");

        $this->dispatcher->forward([
            'controller' => "episode",
            'action' => 'index'
        ]);
    }

    /**
     * Metodo que elimina un item seleccionado
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $episode = Episode::findFirstByid($id);
        if (!$episode) {
            $this->flash->error("episode was not found");

            $this->dispatcher->forward([
                'controller' => "episode",
                'action' => 'index'
            ]);

            return;
        }

        if (!$episode->delete()) {

            foreach ($episode->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "episode",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("episode was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "episode",
            'action' => "index"
        ]);
    }

}
