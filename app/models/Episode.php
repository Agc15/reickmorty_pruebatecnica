<?php

class Episode extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(column="episode", type="string", length=255, nullable=false)
     */
    public $episode;

    /**
     *
     * @var string
     * @Column(column="gender", type="string", length=255, nullable=true)
     */
    public $gender;

    /**
     *
     * @var string
     * @Column(column="specie", type="string", length=255, nullable=true)
     */
    public $specie;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("ricky_and_morty");
        $this->setSource("episode");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'episode';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Episode[]|Episode|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Episode|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
