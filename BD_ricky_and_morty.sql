-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 29-03-2020 a las 20:04:52
-- Versión del servidor: 10.3.16-MariaDB
-- Versión de PHP: 7.2.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ricky_and_morty`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `episode`
--

CREATE TABLE `episode` (
  `id` int(11) NOT NULL,
  `episode` varchar(255) COLLATE utf8_bin NOT NULL,
  `gender` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `specie` varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `episode`
--

INSERT INTO `episode` (`id`, `episode`, `gender`, `specie`) VALUES
(1, 'Prueba', 'Prueba', 'Prueba'),
(2, 'Test', 'Male', 'Test'),
(3, 'Rick Sanchez', 'Male', 'Human'),
(4, 'Morty Smith', 'Male', 'Human'),
(5, 'Summer Smith', 'Female', 'Human'),
(6, 'Beth Smith', 'Female', 'Human'),
(7, 'Jerry Smith', 'Male', 'Human'),
(8, 'Abadango Cluster Princess', 'Female', 'Alien'),
(9, 'Abradolf Lincler', 'Male', 'Human'),
(10, 'Adjudicator Rick', 'Male', 'Human'),
(11, 'Agency Director', 'Male', 'Human'),
(12, 'Alan Rails', 'Male', 'Human'),
(13, 'Albert Einstein', 'Male', 'Human'),
(14, 'Alexander', 'Male', 'Human'),
(15, 'Alien Googah', 'unknown', 'Alien'),
(16, 'Alien Morty', 'Male', 'Alien'),
(17, 'Alien Rick', 'Male', 'Alien'),
(18, 'Amish Cyborg', 'Male', 'Alien'),
(19, 'Annie', 'Female', 'Human'),
(20, 'Antenna Morty', 'Male', 'Human'),
(21, 'Antenna Rick', 'Male', 'Human'),
(22, 'Ants in my Eyes Johnson', 'Male', 'Human'),
(23, 'Aqua Morty', 'Male', 'Humanoid'),
(24, 'Aqua Rick', 'Male', 'Humanoid'),
(25, 'Arcade Alien', 'Male', 'Alien'),
(26, 'Armagheadon', 'Male', 'Alien'),
(27, 'Armothy', 'Male', 'unknown');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `episode`
--
ALTER TABLE `episode`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `episode`
--
ALTER TABLE `episode`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
